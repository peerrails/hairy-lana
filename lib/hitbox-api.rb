module HitboxAPI
  require 'hitbox-api/client'
  require 'hitbox-api/request'
  require 'hitbox-api/entities/base'
  require 'hitbox-api/entities/user'
  require 'hitbox-api/entities/games'
  require 'hitbox-api/entities/followers'
  require 'hitbox-api/entities/team'
  require 'hitbox-api/entities/teams'
  require 'hitbox-api/entities/media'

  def self.client(options = {})
    HitboxAPI::Client.new(options = {})
  end
end
