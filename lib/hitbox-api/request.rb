require "yajl"
require "net/http"
require "uri"

class HitboxAPI::Request
  BASE_ENDPOINT = "http://api.hitbox.tv"

  def self.request(path, params)
    path = path.clone
    params = params.clone

    path = path.split("/").collect do |part|
      if part[0] == ":"
        params[part[1..-1].to_sym]
        params.delete(part[1..-1].to_sym)
      else
        part
      end
    end
    path = path.join("/")

    uri = URI("#{BASE_ENDPOINT}/#{path}?#{self.query(params)}")

    response = Net::HTTP.start(uri.host, uri.port) do |http|
      http.use_ssl = uri.scheme == "https"
      request = Net::HTTP::Get.new(uri)
      http.request(request)
    end

    begin
      @data = Yajl::Parser.parse(response.body, symbolize_keys: true)
    rescue
      @data = {}
    end
  end

  def self.query(params)
    params.map { |k, v| "#{URI.escape(k.to_s)}=#{URI.escape(v.to_s)}" }.join("&")
  end
end
