class HitboxAPI::User < HitboxAPI::Base
  attributes :user_name, :user_cover, :user_status, :user_logo,
    :user_logo_small, :user_is_broadcaster, :followers

  def initialize(options = {})
    super

    @attr = HitboxAPI::Request.request("user/:user_name", params)
  end
end
