class HitboxAPI::Base
  attr_reader :params, :options

  BASE_ENDPOINT = "http://api.hitbox.tv"

  class << self
    def attributes(*attr)
      attr.each do |attr|
        define_method(attr) do
          @attr ||= {}
          @attr[attr]
        end
      end
    end
  end

  def initialize(options = {})
    @options = options.clone
    @params = options.clone
  end
end
