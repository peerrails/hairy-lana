class HitboxAPI::Media < HitboxAPI::Base
  def initialize(options = {})
    super

    @attr = HitboxAPI::Request.request("media/:type/:name", params)
  end
end
