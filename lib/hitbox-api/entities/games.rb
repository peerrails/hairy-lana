class HitboxAPI::Games < HitboxAPI::Base
  def initialize(options = {})
    super

    @attr = HitboxAPI::Request.request("games", params)
  end
end
