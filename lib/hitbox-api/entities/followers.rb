class HitboxAPI::Followers < HitboxAPI::Base
  def initialize(options = {})
    super

    @attr = HitboxAPI::Request.request("followers/user/:user_name", params)
  end
end
