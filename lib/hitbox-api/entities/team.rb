class HitboxAPI::Team < HitboxAPI::Base
  def initialize(options = {})
    super

    @attr = HitboxAPI::Request.request("team/:team", params)
  end
end
