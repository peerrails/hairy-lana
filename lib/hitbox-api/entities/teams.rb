class HitboxAPI::Teams < HitboxAPI::Base
  def initialize(options = {})
    super

    @attr = HitboxAPI::Request.request("teams", params)
  end
end
