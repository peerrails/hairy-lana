class HitboxAPI::Client
  def initialize(options = {})
    @options = options.clone
  end
  
  def games(options = {})
    HitboxAPI::Games.new(@options.merge(options))
  end

  def followers(options = {})
    HitboxAPI::Followers.new(@options.merge(options))
  end

  def media(options = {})
    HitboxAPI::Media.new(options.merge(options))
  end

  def team(options = {})
    HitboxAPI::Team.new(@options.merge(options))
  end

  def teams(options = {})
    HitboxAPI::Teams.new(@options.merge(options))
  end

  def user(options = {})
    HitboxAPI::User.new(@options.merge(options))
  end
end
