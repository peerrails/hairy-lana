require "pp"
require "hitbox-api"

# this isn't a real key at all
# and the current library does nothing with it anyway
HITBOX_API_KEY = "63d935b963fe9760984a806d257a0c6d"
HITBOX_USERNAME = "syphoxy"

# Basic Usage
#
# api       = HitboxAPI.client(api_key: HITBOX_API_KEY)
# user      = api.user(user_name: HITBOX_USERNAME)
# followers = api.followers(user_name: "saboom")
# games     = api.games
# media     = api.media
# team      = api.team(team: "DotA")
# teams     = api.teams
