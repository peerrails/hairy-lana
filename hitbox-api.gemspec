Gem::Specification.new do |s|
  s.name        = "hitbox-api"
  s.version     = "0.0.0"
  s.date        = "2014-08-11"
  s.summary     = "Easy interface to hitbox.tv API"
  s.description = "Easy interface to hitbox.tv API"
  s.authors     = ["Dario Russo"]
  s.email       = "me@dariorusso.ca"
  s.files       = ["lib/hitbox-api.rb"]
  s.homepage    = "http://dariorusso.ca/projects/hitbox-api"
  s.license     = "WTFPL"
end
